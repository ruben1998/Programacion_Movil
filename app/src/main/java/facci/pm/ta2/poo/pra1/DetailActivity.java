package facci.pm.ta2.poo.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.DataQuery;
import facci.pm.ta2.poo.datalevel.GetCallback;
import android.widget.TextView;
import android.widget.ImageView;

public class DetailActivity extends AppCompatActivity {

    TextView Precio, Descripcion, Nombre; ImageView IMAGEN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6
        // Se crean las variables y se las invoca
        Descripcion = (TextView)findViewById(R.id.descripcion);
        IMAGEN = (ImageView)findViewById(R.id.imagen);
        Precio = (TextView) findViewById(R.id.precio);
        Nombre = (TextView)findViewById(R.id.nombre);

      //Accediendo al object_id recibido como parámetro en la actividad

        final DataQuery query = DataQuery.get("item");
        //recibir parametros para la interfaz Activity_detail
        String parametro = getIntent().getExtras().getString("OBJETOCLASE");
        query.getInBackground(parametro, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e==null){
                    // Se invocan los parametros
                    String precio = (String) object.get("price")+("\u0024");
                    String descripcion = (String) object.get("description");
                    String nombre = (String) object.get("name");
                    Bitmap imagen = (Bitmap) object.get("image");
                    //Se generan los valores
                    Precio.setText(precio);
                    Descripcion.setText(descripcion);
                    Nombre.setText(nombre);
                    IMAGEN.setImageBitmap(imagen);

                }else {
                    //error
                }
            }
        });



        // FIN - CODE6

    }

}
